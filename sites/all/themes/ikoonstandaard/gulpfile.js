var gulp    = require('gulp'),
    less    = require('gulp-less'),
    path    = require('path'),
    watch   = require('gulp-watch');

gulp.task('default', ['less', 'fonts'], function(){
});

gulp.task('watch', function(){
    gulp.watch('./less/*.less', ['less']);
});

gulp.task('less', function () {
    return gulp.src('./less/style.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./css'));
});

gulp.task('fonts', function(){
    gulp.src('./bootstrap/fonts/*')
        .pipe(gulp.dest('./fonts/'))
});

// Add debounce to gulp watch for FTP
(function ftp_debounce_fix(){

    var watch = gulp.watch;

    // Overwrite the local gulp.watch function
    gulp.watch = function(glob, opt, fn){
        var _this = this, _fn, timeout;

        // This is taken from the gulpjs file, but needed to
        // qualify the "fn" variable
        if ( typeof opt === 'function' || Array.isArray(opt) ) {
            fn = opt;
            opt = null;
        }

        // Make a copy of the callback function for reference
        _fn = fn;

        // Create a new delayed callback function
        fn = function(){

            if( timeout ){
                clearTimeout( timeout );
            }

            timeout = setTimeout( Array.isArray(_fn) ? function(){
                _this.start.call(_this, _fn);
            } : _fn, 300 );

        };

        return watch.call( this, glob, opt, fn );
    };

})();